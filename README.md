# Lempix

Debian focused script server to perform regular operation on a web server.
- Basic pakage (vim, git, LEMP Stack)
- Manage usual Nginx & MySQL
- Easy install of WordPress && Nextcloud
- Easy to add new functionalities


## Install

The script is made to live in `/var/lib/`
Then a symlink in `/usr/sbin` since most of the command are to be run as root.

Clone this repository and then run :

```
chmod +x install.sh
./install.sh
```


## Usage

```
ssk help
```
Display some basic help

### Install commands

Install commands are in the form of

```
ssk install PARAMETERS
```

The goal of the install command series is not to replace apt-get but to create
a layer abstracting the complexity around dependancies to really get one-liner
for installing software on your server.

| Parameters | Description |
| --- | --- |
| basics | Install basics (apt utils)|
| sysupdate | update & upgrade & clean |
| stretch | upgrade to debian to strech |
| vim | Install vim, Vundle, compile completion |
| ssl | Install letsencrypt |
| lemp | Install Mariadb, Nginx, Php7, Letsencrypt, Wp Cli|
| net-monitoring | saidar, mpstat, iftop, iptraf, nmap, nload, nethogs, nmon|
| redis | pretty obviously - will install redis|
| nextcloud | install nextcloud |


### Add / Del sites

```
ssk add site SITETYPE SITEURL
ssk del site SITETYPE SITEURL
```


| SITETYPE | Description |
| --- | --- |
| html | Create HTML site - no database - no ssl |
| shtml | Create HTML site - no database - SSL |
| php | Create PHP site - no database - no ssl |
| sphp | Create PHP site - no database - no ssl |
| wp | Create WordPress site - Database - no ssl |
| swp | Create WordPress site - Database - no ssl |

### Database operations

**Create a new database**

```
ssk add db DBNAME USERNAME PASSWORD
```

**Create a new database in interactive mode**

```
ssk add dbi
```

**import db**

```
ssk add dump DUMPFILE
```
