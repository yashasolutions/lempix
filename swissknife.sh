#/bin/bash
cd $(dirname $0)


#Run as a root
SSK_DIRRUNTIME=${PWD}
SSK_DIRINSTALL=$(dirname $0)
SSK_INSTALLIB="/var/lib/serverswissknife" #Change in case you to install
SSK_LIB=$SSK_INSTALLIB/"lib"
SSK_SETTING=$SSK_INSTALLIB/"lib/settings"
SSK_CONFIGS=$SSK_INSTALLIB/"lib/configs"
SSK_SNIPPETS=$SSK_INSTALLIB/"lib/snippets"
SSK_WEBDIR="/var/www"
SSK_NGDIR="/etc/nginx"

. $SSK_INSTALLIB/inc/basics.sh
. $SSK_INSTALLIB/inc/nextcloud.sh
. $SSK_INSTALLIB/inc/nginx.sh
. $SSK_INSTALLIB/inc/db.sh
. $SSK_INSTALLIB/inc/helpers.sh
. $SSK_INSTALLIB/inc/network.sh
. $SSK_INSTALLIB/inc/wordpress.sh



function add_site {
	siteurl=$2
	sitetype=$1

	#Checking argument for domain name was given
	if [ -z "$siteurl" ]; then
		echo "Please specify a domain name for your site"
		return;
		#TODO: Ask for user input
	fi


	case $sitetype in
		html | shtml | php | sphp | wp | swp)
			echo "Creating $sitetype site $siteurl"
			echo "Proceed?"
			if isno; then
				echo "Aborting"
				return;
			fi
			;;
		*)
			echo "Unknown site type aborting"
			;;
	esac

	echo "........."

	ng_site_structure $siteurl

	case $sitetype in
		shtml | sphp | swp)
			ng_site_ssl_cert $siteurl
		;;
	esac

	ng_site_config $sitetype $siteurl

	case $sitetype in
		wp | swp)
			wp_install $sitetype $siteurl
			;;
	esac

	chown -R www-data:www-data $SSK_WEBDIR/$siteurl/web

}



function del_site {
	siteurl=$2
	sitetype=$1

	#Checking argument for domain name was given
	if [ -z "$siteurl" ]; then
		echo "Please specify a domain name for your site"
		return;
		#TODO: Ask for user input
	fi


	case $sitetype in
		html | shtml | php | sphp | wp | swp | nextcloud)
			echo "Deleting $sitetype site $siteurl"
			;;
		*)
			echo "Unknown site type aborting"
			;;
	esac

	echo "........."

	echo "Deleting $siteurl"
	echo "Are you sure?"
	if isyes; then
		echo "Deleting site configuration"
		rm $SSK_NGDIR/sites-available/$siteurl.conf
		rm $SSK_NGDIR/sites-enabled/$siteurl.conf
		echo "Deleting site repository"
		rm -rf $SSK_WEBDIR/$siteurl
	fi

	echo "Done"
	echo "    "
	sleep 1

	case $sitetype in
		wp | swp | nextcloud)
			echo "It is a dynamic site - there is a database"
			echo "Do you want to backup / delete the database?"
			if isyes; then
				mysql -e "show databases;"
				echo "Which database to backup / delete?"
				read dbname
				echo "Shall we maybe backup DB $dbname?"
				if isyes; then
					mysqldump $dbname > /tmp/$dbname.sql.dump
				fi
				echo "Proceeding with DB $dbname deletion?"
				if isyes; then
					echo "Boom"
					mysql -e "DROP DATABASE IF EXISTS ${dbname};"
				fi
				echo "Proceeding with DB user deletion?"
				if isyes; then
					mysql -e "SELECT User FROM mysql.user;"
					echo "Which user shall we delete?"
					read username
				    mysql -e "DROP USER ${username}@localhost ;"
				fi
			fi
			;;
		*)
			;;
	esac

	echo "Done"

}

###########################
## SSH function
###########################


function list_assets {
	asset_name=$1

	case $asset_name in
		sites)
			ls -l /etc/nginx/sites-enabled
			;;
		db)
			mysql -e "show databases;"
			;;
		dbusers)
			mysql -e "SELECT User FROM mysql.user; "
			;;
		*)
			echo "Wut m8?"
			;;
	esac


}


#################
## Init Script ##
#################


case "$1" in
	# Install Packages
	help)
		display_help
		;;

	about)
		cat $SSK_LIB/about.txt
		;;

	install)
		case "$2" in
			basics)
			 	install_basics
				;;
			sysupdate)
				install_sysupdate
				;;
			stretch)
				install_debian_update
				;;
			vim)
				menu_vim
				;;
			config)
				echo "Deploying script configs and new snippets"
				deploy_script
				deploy_snippets
				echo "Done"
				;;
			ssl)
				install_letsencrypt
				;;

			lemp)
				install_mariadb
				install_php7
				install_nginx
				install_letsencrypt
				install_wpcli
				;;
			wpcli)
				install_wpcli
				;;

			nextcloud)
				echo "Requirements"
				nextcloud_requirements
				nextcloud_download
				nextcloud_install $3
				;;
			nc-setup)
				nextcloud_setup $3
				;;
			nc-redis)
					install_redis
					nextcloud_redis_config $SSK_WEBDIR/$3/web/config/config.php
				;;
			nc-cron)
				nextcloud_setup_cron $3
				;;
			net-monitoring)
				net_monitoring_basics_install

				;;
			redis)
				install_redis
				echo "done"
				;;
			*)
				echo "Installed nothing"
				;;
		esac

		;;


	add)
		case "$2" in
			site)
				add_site $3 $4
				;;
			db)
				#dbname usernam password
				db_create_new $3 $4 $5
				;;
			dbi)
				db_create_interactive
				;;
			dump)
				db_import_dump $3
				;;
			*)
				echo $2
				;;
		esac
		;;

	del)
		case "$2" in
			site)
				del_site $3 $4
				;;
			*)
				echo $2
				;;
		esac
		;;
	show)
		case "$2" in
			os)
				uname -a
				cat /etc/*-release
				cat /proc/version
				;;
			*)
				list_assets $2 $3
				;;
		esac
		;;

	backup)
		case "$2" in
			wp)
				wp_backup $3 $4
				;;
			*)
				echo "yet to be done"
				;;
		esac
		;;
	ssh)
		echo "not done yet"
		;;

	edit)
		vim $SSK_INSTALLIB/swissknife.sh
		;;
	clear)
		clean_crash
		;;

	update)
		cd $SSK_INSTALLIB/
		git pull

		;;

	*)
		display_help
		;;

esac
