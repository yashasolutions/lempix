
function menu_vim {
	echo "You are about to install VIM"
	echo "This might take a while"
	if isyes; then
		echo "Let's do it!"
		install_vim
	else 
		echo "Skipping that one!"
	fi 
}


function isno {

	echo -n "Yes / No (Y/N)? "
	while read -r -n 1 -s answer; do
		if [[ $answer = [YyNn] ]]; then
			[[ $answer = [Yy] ]] && retval=1
			[[ $answer = [Nn] ]] && retval=0
			break
		fi
	done

	printf "${answer}\n"
	return $retval

}

function isyes {

	echo -n "Yes / No (Y/N)? "
	while read -r -n 1 -s answer; do
		if [[ $answer = [YyNn] ]]; then
			[[ $answer = [Yy] ]] && retval=0
			[[ $answer = [Nn] ]] && retval=1
			break
		fi
	done

	printf "${answer}\n"
	return $retval

}



function display_help {

	echo "Swiss knife for server management"
	echo "Options:" 
	echo -e "help \t\t\t\tDisplay this help"
	echo -e "about\t\t\t\tDisplay the about text "
	echo ""
	echo -e "install"
	echo -e "\tvim \t\t\t\tInstall a VIM IDE"
	echo -e "\tlemp \t\t\t\tInstall a lemp server"
	echo -e "\tconfig \t\t\t\tInstall only vim config / snippets"
	echo -e "\twpcli \t\t\t\tInstall wpcli"
	echo -e "\tnextcloud\t\t\tInstall Nextcloud"
	echo -e "list"
	echo -e "\tsites \t\t\t\tList sites enabled"
	echo -e "\tdb \t\t\t\tList dbs"
	echo -e "\tdbusers \t\t\t\tList db users"
	echo -e "add | del"
	echo -e "\tsite [(s)html|(s)php|(s)wp] URL \t<-- What it says "
	echo " "

}

