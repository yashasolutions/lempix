function nextcloud_requirements {
	dependancies=(ctype dom GD iconv JSON libxml mbstring posix SimpleXML XMLReader XMLWriter zip zlib curl fileinfo bz2 intl mcrypt openssl imap ftp exif gmp)
	for i in ${dependancies[@]}; do
	   #echo $i
	   if ! php -m | grep -iq $i; then
		   echo "$i is missing"
		   echo "install?"
		   if isyes; then
			   apt-get -qq install php-$i
			   echo "php-$i installed"
		   fi  
	   fi
   	done
	echo "All nextcloud dependancies ok"

}

function nextcloud_download {
	nc_base_dl="https://download.nextcloud.com/server/releases"
	echo "do you want do download nextcloud latest?"
	if isyes; then
		wget $nc_base_dl/latest.zip -P /tmp/
		mv /tmp/latest.zip /tmp/NC.zip 
	else
		echo "which version?"
		read nc_version
		wget $nc_base_dl/nextcloud-$nc_version.zip -P /tmp/
		mv /tmp/nextcloud-$nc_version.zip /tmp/NC.zip 
	fi
	echo "All was downloaded"



}

function nextcloud_install {
	siteurl=$1 
	ng_site_structure $siteurl  
	ng_site_config "html" $siteurl 
	ng_site_ssl_cert $siteurl 

	ng_site_config "nextcloud" $siteurl 	
	
	rm -rf /tmp/nextcloud 
	unzip /tmp/NC.zip -d /tmp/
	mv /tmp/nextcloud/* $SSK_WEBDIR/$siteurl/web/ 
	mv /tmp/nextcloud/.[a-z]* $SSK_WEBDIR/$siteurl/web/
	
	#add_site "nextcloud" $siteurl 
	chown -R www-data:www-data $SSK_WEBDIR/$siteurl/web/

	prjname=${siteurl//./_}
	username=$prjname
	dbname="db_$prjname"
	pass="Printer#42"

	db_create_new $dbname $username $pass 
	
	cd $SSK_WEBDIR/$siteurl/web/
	sudo -u www-data php occ  maintenance:install --database "mysql" --database-name $dbname --database-user $username --database-pass $pass       --admin-user "admin" --admin-pass "password"

	nextcloud_setup $siteurl 
	nextcloud_setup_cron $siteurl 

	echo "====================="
	echo " "
	echo "installed nextcloud" 
	echo "lets install redis?"
	if isyes; then 
		install_redis
	fi
	echo "lets config redis for nextcloud ?"
	if isyes; then 
		nextcloud_redis_config $SSK_WEBDIR/$siteurl/web/config/config.php 
	fi

}

function nextcloud_setup {
	siteurl=$1 

	cd $SSK_DIRUNTIME 

	echo "moved back to $SSK_DIRRUNTIME"
	
	sed -i "s/0 => 'localhost'/0 => '$siteurl'/g" $SSK_WEBDIR/$siteurl/web/config/config.php 

	echo "env[PATH] = /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin" >> /etc/php/7.0/fpm/pool.d/www.conf

	cat $SSK_DIRRUNTIME/$SSK_SETTING/nextcloud/snippets_phpini >> /etc/php/7.0/fpm/php.ini 

	service nginx reload 
	service php7.0-fpm reload 


}


function nextcloud_config_redis {
	siteurl=$1 

}


function install_redis {


	#verify install 
	sudo apt-get update && sudo apt-get install build-essential -y
	sudo apt-get install tcl8.5 -y


	#Install reddis
	sudo wget http://download.redis.io/releases/redis-stable.tar.gz && sudo tar xzf redis-stable.tar.gz
	sudo mv redis-stable redis
	cd redis && sudo make && taskset -c 1 sudo make test
	sudo make install

	#in UTILS
	cd utils && sudo ./install_server.sh

	#testing 
	/usr/local/bin/redis-server -v

	#INSTALL NEEDED DEPENDENCIES
	sudo apt-get install php7.0-dev -y


	#Install Git and clone repo
	sudo git clone -b master https://github.com/phpredis/phpredis.git

	#Move phpredis to /etc
	sudo mv phpredis/ /etc/ && cd /etc/phpredis

	#Build the phpmodule with make
	sudo phpize && sudo ./configure && sudo make && sudo make install

	#Add the extension to php.ini
	sudo touch /etc/php/7.0/mods-available/redis.ini
	sudo echo 'extension=redis.so' > /etc/php/7.0/mods-available/redis.ini


	#Enable the extension and restart
	sudo phpenmod redis 
   	sudo service nginx restart
   	sudo service php7.0-fpm restart

	#Remove the dir we built it from, not needed anymore
	cd .. && rm -rf phpredis


	#TEST YOUR MODULE VERSION
	php --ri redis

	cd $SSK_DIRRUNTIME 

	rm -rf $SSK_DIRRUNTIME/redis*


}


function nextcloud_redis_config {
	sed -i "/);/r $SSK_SETTING/nextcloud/snippets_nc_conf_redis" $1
	sed -i "/);/d" $1
	echo ");" >> $1
}


