function net_monitoring_basics_install {


	apt-get install -y saidar;
	apt-get install -y mpstat;
	apt-get install -y iftop
	apt-get install -y iptraf
	apt-get install -y nmap
	apt-get install -y nload
	apt-get install -y nethogs
	apt-get install -y nmon


}

function net_dns {
	
	apt-get install -y dnsutils

}
