
function install_nginx {
	echo "=== Installing nginx"
	sudo apt-get -qq install net-tools sudo wget curl bash-completion
	sudo apt-get -qq install nginx 
	rm /etc/nginx/sites-enabled/default
	echo "=== Installing nginx: Done"
}

function ng_site_structure {
	siteurl=$1 
	#create the site structure 	
	echo "Creating site structure"
	sudo mkdir -p $SSK_WEBDIR/$siteurl/web/
	sudo mkdir -p $SSK_WEBDIR/$siteurl/log/
	sudo mkdir -p $SSK_WEBDIR/$siteurl/ssl/
	chown -R www-data:www-data $SSK_WEBDIR/$siteurl 

}

function ng_site_config {
	siteurl=$2
	sitetype=$1

	#Create nginx config
	echo "deploying nginx configuration"
	echo "SITE URL : "$siteurl 
	echo "SITE TYPE: "$sitetype 

	cp -f $SSK_SETTING/nginx/$sitetype.conf $SSK_NGDIR/sites-available/$siteurl.conf
	sed -i "s/SSKURL/$siteurl/g" $SSK_NGDIR/sites-available/$siteurl.conf

	echo "Enabling site"
	ln -s $SSK_NGDIR/sites-available/$siteurl.conf $SSK_NGDIR/sites-enabled/$siteurl.conf 
	echo "nginx reload"
	sudo service nginx reload 

}

function ng_site_ssl_cert {
	siteurl=$1 
	echo "Requesting the SSL certificate for $siteurl" 
	certbot certonly --authenticator standalone -d $siteurl --pre-hook "nginx -s stop" --post-hook "nginx" 

}

function ng_site_ssl_cert_auto_renew {
	
	echo "Add automatic renewal"
	#adding ssl renewable to the cron
	crontab -l > /tmp/cronssl
	echo "0  5    * * *   root     /usr/bin/certbot renew --quiet" >> /tmp/cronssl
	crontab /tmp/cronssl 
	rm /tmp/cronssl 

	echo "SSL certificates done" 

}


function ng_site_ssl_convert {
	siteurl=$1

	sed -i "/#SSKSSL82REDIRECT/r $SSK_SETTING/nginx/sslsnippet_80redirect" $SSK_NGDIR/site-available/$siteurl.conf 
	sed -i "/#SSKSSLKEY/r $SSK_SETTING/nginx/sslsnippet_keys" $SSK_NGDIR/site-available/$siteurl.conf 

	#more sed stuff to remove the markers 
}
