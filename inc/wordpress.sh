
function wp_install {
	siteurl=$2
	sitetype=$1 

	echo "Creating database";
	prjname=${siteurl//./_}
	username=$prjname
	dbname="db_$prjname"
	echo "About to create DB $dbname with user $username"
	echo "Please specify password"
	read userpass 
	charset="utf8mb4"	
	mysql -e "CREATE DATABASE ${dbname} /*\!40100 DEFAULT CHARACTER SET ${charset} */;"
	mysql -e "show databases;"
	mysql -e "CREATE USER ${username}@localhost IDENTIFIED BY '${userpass}';"
	mysql -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${username}'@'localhost';"
	mysql -e "FLUSH PRIVILEGES;"

	echo "Fresh install?"
	if isyes; then 
		echo "Downloading wordpress"
		sudo -u www-data wp core download -path=$SSK_WEBDIR/$siteurl/web/ 
		cp $SSK_SETTING/wp/local-config.php $SSK_WEBDIR/$siteurl/web/
		echo "local-config.php" >>  $SSK_WEBDIR/$siteurl/web/.gitignore 
		sed -i "s/SSKDB/$dbname/g"  $SSK_WEBDIR/$siteurl/web/local-config.php 
		sed -i "s/SSKUSERDB/$username/g"  $SSK_WEBDIR/$siteurl/web/local-config.php 
		sed -i "s/SSKPWDDB/$userpass/g"  $SSK_WEBDIR/$siteurl/web/local-config.php 
		echo "Done"
	else
		echo "Git deploy"?
		if isyes; then
			echo "Input path to repo"
			read gitrepo 
			#git clone $SSK_GITREPO/$subrepo $SSK_WEBDIR/$siteurl/web 
			git clone $gitrepo $SSK_WEBDIR/$siteurl/web 
			cp $SSK_SETTING/wp/local-config.php $SSK_WEBDIR/$siteurl/web/
			echo "local-config.php" >>  $SSK_WEBDIR/$siteurl/web/.gitignore 
			sed -i "s/SSKDB/$dbname/g"  $SSK_WEBDIR/$siteurl/web/local-config.php 
			sed -i "s/SSKUSERDB/$username/g"  $SSK_WEBDIR/$siteurl/web/local-config.php 
			sed -i "s/SSKPWDDB/$userpass/g"  $SSK_WEBDIR/$siteurl/web/local-config.php 
			echo "done"
		fi
	fi

}

function wp_backup {
	wpdir=$1
	if [ -z "$wpdir" ]; then
	   wpdir="."
   	fi

	wpuser=$2	
	if [ -z "$wpuser" ]; then
	   wpuser="www-data"
   	fi

	cd $wpdir
	backuppath="backup"
	dbpath="$backuppath/SQL"
	dbnamepath="$dbpath/db$(date +"%y%m%d_%H-%M-%S").sql"

	sudo -u $wpuser mkdir -p $wpdir/$dbpath 
	
	sudo -u $wpuser wp db export $wpdir/$dbnamepath --path=$wpdir
	sudo -u $wpuser tar -czvf $wpdir/$backuppath/fullinstall.tgz $wpdir --exclude=$wpdir/$backuppath  
	#TODO : add naming with timestamp
	#TODO : make a function just for db backup 

}

