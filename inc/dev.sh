function dev_nodejs_install {

	curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash 
	apt-get -y install nodejs
	apt-get -y  install build-essential libssl-dev

}

function dev_composer_install {

	apt-get install composer 
}
