
function clean_crash {
    rm $SSK_INSTALLIB/inc/.*.swp;
	rm $SSK_INSTALLIB/.*.swp;

}

function install_basics {
	apt-get -qq install sudo apt-utils
}

function install_debian_update {
	apt-get dist-upgrade
	cp /etc/apt/sources.list /etc/apt/sources.list_backup
	cp /etc/apt/sources.list /tmp/sources.list_backup-$(date +"%Y%m%d_%H%M%S") #backup with time stamp in case shit goes wrong
	sed -i "s/jessie/stretch/g"  /etc/apt/sources.list
}


function install_sysupdate {

	apt-get update
	apt-get upgrade
	apt-get clean
}

function install_vim {

  deploy_snippets
	# Getting vim to work
	echo ">>> Installing vim as an IDE"
	sudo apt-get -qq install vim vim-nox cmake gcc g++ python-dev python3-dev
	echo "====> PACKGINE DONE ==="
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	cp $SSK_CONFIGS/vimrc ~/.vimrc
	echo "====> VUNDLE DONE ==="
	vim lib/vim.md
	python ~/.vim/bundle/youcompleteme/install.py --js-completer
	#TODO: add gocode support
	echo "====> YCM DONE ==="
	cp $SSK_SNIPPETS/snippets.json ~/.vim/snippets.json
	echo "====> VIM DONE ==="
}



function install_wpcli {

	curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
	echo "downloaded"
	echo "Test?"

	if isyes;	then
		mv wp-cli.phar /tmp/
		sudo -u www-data php /tmp/wp-cli.phar --info
	fi
	echo "Install?"

	if isyes;	then
		chmod +x /tmp/wp-cli.phar
		sudo mv /tmp/wp-cli.phar /usr/local/bin/wp
	fi

}


function install_letsencrypt {
	# for nginx
	sudo apt-get -qq install certbot
	sudo apt-get -qq install python-certbot
	sudo apt-get -qq install python-certbot-nginx
	sudo apt-get -qq install letsencrypt

}

function deploy_script {
	cp $SSK_CONFIGS/bash_profile ~/.bash_profile
	cp $SSK_CONFIGS/vimrc ~/.vimrc
	source ~/.bash_profile
}

function deploy_snippets {
	cp $SSK_SNIPPETS/snippets.json ~/.vim/snippets.json
	cp $SSK_SNIPPETS/vim-snippets/snippets/wordpress.snippets ~/.vim/bundle/vim-snippets/snippets/wordpress.snippets
}



function install_php7 {
	echo "=== Installing PHP7"
	sudo apt-get -qq install  php7.0-fpm php7.0-mysql php7.0-common php7.0-gd php7.0-json php7.0-cli php7.0-curl php7.0-xml php7.0-zip php7.0-mbstring php7.0-sqlite php7.0-mcrypt
	echo "=== PHP7 - Done"
}
