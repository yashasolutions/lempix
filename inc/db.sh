function install_mariadb {
	echo "=== Installing Maria DB"
	sudo apt-get -qq install  mariadb-server mariadb-client
	echo "Do want to configure it now?"
	if isyes; then
		sudo mysql_secure_installation
	else 
		echo "Do not forget to run mysql_secure_installation later"
	fi 
	invoke-rc.d mysql stop
	invoke-rc.d mysql start
	echo "=== Installing Maria DB: DONE"
}

function db_create_new {
			echo "Creating database";
			username=$2
			dbname=$1 
			userpass=$3 

			echo "About to create DB $dbname with user $username"
		
			mysql -e "CREATE DATABASE ${dbname} CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci; "
			mysql -e "show databases;"
			mysql -e "CREATE USER ${username}@localhost IDENTIFIED BY '${userpass}';"
			mysql -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${username}'@'localhost';"
			mysql -e "FLUSH PRIVILEGES;"

			echo "Done" 
}

function db_create_new_user_for_db {
			username=$2
			dbname=$1 
			userpass=$3 

			echo "About to create user $username for $dbname"
		
			mysql -e "CREATE USER ${username}@localhost IDENTIFIED BY '${userpass}';"
			mysql -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${username}'@'localhost';"
			mysql -e "FLUSH PRIVILEGES;"

			echo "Done" 
}

function db_create_new_user {
			username=$1
			userpass=$2 

			echo "About to create user $username"
		
			mysql -e "CREATE USER ${username}@localhost IDENTIFIED BY '${userpass}';"

			echo "Done" 
		
}

function db_grant_user_to_db {
			username=$2
			dbname=$1 

			echo "About to grant all to user $username for $dbname"
		
			mysql -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${username}'@'localhost';"
			mysql -e "FLUSH PRIVILEGES;"

			echo "Done" 
}

function db_create_interactive {
	echo "Let's make a new user?";
	if isyes; then 
		echo "List of existing users"
		mysql -e "SELECT User FROM mysql.user;"
		echo "what username for this new user?"
		read username
		echo "what password for this new user?"
		read userpass 
	fi
	echo "Ok."
	echo "Now what name for the db?"
	read dbname

	db_create_new $dbname $username $userpass 

}

function db_create_new_db {
			dbname=$1 

			echo "About to create DB $dbname "
		
			mysql -e "CREATE DATABASE ${dbname} CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci; "

			echo "Done" 
}



function db_import_dump {
	dumpfile=$1 
	echo "Let's make a new user?";
	if isyes; then 
		echo "List of existing users"
		mysql -e "SELECT User FROM mysql.user;"
		echo "what username for this new user?"
		read username
		echo "what password for this new user?"
		read userpass 
		db_create_new_user $username $userpass 
	else
		echo "Select on user from the List of existing users"
		mysql -e "SELECT User FROM mysql.user;"
		read username
		echo "what is the  password for this user?"
		read userpass 
	fi 

	echo "Ok."

	echo "Let's make a new db?";

	if isyes; then 
		echo "Now what name for the db?"
		read dbname
		db_create_new_db $dbname 
	else 
		echo "Now what name for the db?"
		mysql -e "show databases;"
		read dbname
	fi 

	db_grant_user_to_db $dbname $username 

	mysql -u $username -p$userpass -h localhost -D $dbname -o < $dumpfile  	

	#TODO : import dump without creating user and stuff
	echo "Done"
} 
